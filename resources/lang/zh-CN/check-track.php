<?php
return [
    'labels' => [
        'CheckTrack' => '盘点追踪',
    ],
    'fields' => [
        'check_id' => '任务ID',
        'item_id' => '物件',
        'status' => '状态',
        'user' => [
            'name' => '盘点人员'
        ],
        'creator' => '创建者',
    ],
    'options' => [
    ],
];
