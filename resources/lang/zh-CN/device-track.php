<?php
return [
    'labels' => [
        'DeviceTrack' => '设备追踪',
    ],
    'fields' => [
        'device' => [
            'name' => '设备'
        ],
        'staff' => [
            'name' => '雇员'
        ],
    ],
    'options' => [
    ],
];
