<?php
return [
    'labels' => [
        'HardwareTrack' => '硬件追踪',
    ],
    'fields' => [
        'hardware' => [
            'name' => '硬件'
        ],
        'device' => [
            'name' => '设备'
        ],
    ],
    'options' => [
    ],
];
